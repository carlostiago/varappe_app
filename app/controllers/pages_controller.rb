class PagesController < ApplicationController
    def show
   		render template: "pages/#{params[:page]}"
	    @schedulers = Scheduler.where(user_id: current_user.id)
	    @rooms = Room.all
	    @months = Month.all
    end
  end 