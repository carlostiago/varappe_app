class SchedulersController < ApplicationController
  before_action :set_scheduler, only: [:show, :edit, :update, :destroy]


  # GET /schedulers
  # GET /schedulers.json
  def index
    @schedulers = Scheduler.where(user_id: current_user.id)
    @rooms = Room.all
    @months = Month.all
  end

  # GET /schedulers/1
  # GET /schedulers/1.json
  def show
  end

  # GET /schedulers/new
  def new
    @scheduler = Scheduler.new
    @rooms = Room.all
    @months = Month.all
  end

  # GET /schedulers/1/edit
  def edit
    @rooms = Room.all
  end

  # POST /schedulers
  # POST /schedulers.json
  def create
    @rooms = Room.all
    @months = Month.all
    @scheduler = Scheduler.new(scheduler_params)

    if !current_user.admin?
      @scheduler.user_id = current_user.id
    end
    
    @scheduler.month_id = @scheduler.date_scheduler_start.strftime("%m").to_i 
    @scheduler.confirm = true
    @scheduler.repeat_id = @scheduler.scheduler_token
    @scheduler.status = "Agendamento realizado"



    @notification      = Notification.new
    @notification.desc   = @scheduler.status 
    @notification.status   = false
    @notification.user_id = @scheduler.user_id 
    @notification.save

    respond_to do |format|
      if @scheduler.save

        week_scheduler  = @scheduler.date_scheduler_start.cweek 
        week_repeat     = 52 - week_scheduler

        if @scheduler.repeat
            i = 0
            for i in 1..week_repeat do            
              @scheduler_repeat                       = Scheduler.new(scheduler_params)
              
              if !current_user.admin?
                @scheduler_repeat.user_id = current_user.id
              end
              
              @scheduler_repeat.time_scheduler_start  = @scheduler.time_scheduler_start
              @scheduler_repeat.repeat_id             = @scheduler.scheduler_token
              @scheduler_repeat.repeat                = true
              @scheduler_repeat.date_scheduler_start  = @scheduler_repeat.date_scheduler_start + (i*7)
              @scheduler_repeat.month_id              = @scheduler_repeat.date_scheduler_start.strftime("%m").to_i
              mes_agendamento                         = @scheduler_repeat.date_scheduler_start.strftime("%m").to_i
              mes_ativado                             = Month.where(status: true, year_id: 1).last

              if mes_ativado.number < mes_agendamento 
                @scheduler_repeat.reserved = true
                @scheduler_repeat.status                = "Agendamento reservado"
                @scheduler_repeat.confirm = false
              else
                @scheduler_repeat.status                = "Agendamento realizado"
                @scheduler_repeat.confirm = true
              end

              if !@scheduler_repeat.save
                flash[:alert] = "Agendamento para o dia #{@scheduler_repeat.date_scheduler_start.strftime("%d")}/#{@scheduler_repeat.date_scheduler_start.strftime("%m")}/#{@scheduler_repeat.date_scheduler_start.strftime("%Y")} não pode ser realizado."
              end
            end
        end

        format.html { redirect_to schedulers_path, notice: @scheduler.status }
        format.json { render :show, status: :created, location: @scheduler }
      else
        format.html { render :new }
        format.json { render json: @scheduler.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedulers/1
  # PATCH/PUT /schedulers/1.json
  def update

    @notification      = Notification.new


    if  params[:scheduler][:confirm] == "true"
        @scheduler.confirm   = true
        @notification.desc   = 'Agendamento confirmado.'
        @notification.status   = false
        @notification.user_id = @scheduler.user_id 
        @notification.save

        mes_agendamento_number = @scheduler.date_scheduler_start.strftime("%m").to_i
        mes_agendamento = @scheduler.date_scheduler_start.strftime("%m")

        @scheduler.save

        primeiro_dia = Date.civil(@scheduler.date_scheduler_start.strftime("%Y").to_i, @scheduler.date_scheduler_start.strftime("%m").to_i, 1)
        ultimo_dia = Date.civil(@scheduler.date_scheduler_start.strftime("%Y").to_i, @scheduler.date_scheduler_start.strftime("%m").to_i, -1)

        Scheduler.where(
          user_id: @scheduler.user_id,
          date_scheduler_start: primeiro_dia..ultimo_dia,
          time_scheduler_start: @scheduler.time_scheduler_start,
          confirm: false,
          room_id: @scheduler.room_id).update_all(confirm: true, status: 'Agendamento confirmado.' )


        redirect_to schedulers_url, notice: 'Agendamentos confirmados.'
    else
        @scheduler.confirm   = false
        @notification.desc   = 'Agendamento cancelado.'
        @notification.status   = false
        @notification.user_id = @scheduler.user_id 
        @notification.save
        @scheduler.destroy
        redirect_to schedulers_url, notice: 'Agendamento cancelado.'
    end

     
  end

  # DELETE /schedulers/1
  # DELETE /schedulers/1.json
  def destroy

    # data de agendamento igual data agendamento atual e maior se for repeat

    now = Time.now + 24.hour
    hour_date = @scheduler.date_scheduler_start.to_s + ' ' + @scheduler.time_scheduler_start.to_s + ':00'
    if hour_date.to_time >= now
      scheduler_repeat_id_to_destroy = params[:repeat_id]
      scheduler_to_destroy = Scheduler.find(params[:id].to_i)
      if scheduler_to_destroy && scheduler_repeat_id_to_destroy.nil?
        Scheduler.find(scheduler_to_destroy.id).destroy
          @notification      = Notification.new
          @notification.desc   = 'Agendamento cancelado.'
          @notification.status   = false
          @notification.user_id = @scheduler.user_id 
          @notification.save
      elsif scheduler_repeat_id_to_destroy
          last_month = Month.last
          date_to_destroy_end = Date.civil(last_month.year.name, last_month.number, -1).strftime("%Y-%m-%d")
          if Scheduler.find(params[:id].to_i)
            Scheduler.find(params[:id].to_i).destroy
          end
          Scheduler.where(
            repeat_id: scheduler_repeat_id_to_destroy, 
            date_scheduler_start: scheduler_to_destroy.date_scheduler_start.strftime("%Y-%m-%d")..date_to_destroy_end
          ).destroy_all
          @notification      = Notification.new
          @notification.desc   = 'Agendamento cancelado.'
          @notification.status   = false
          @notification.user_id = @scheduler.user_id 
          @notification.save
      else
          scheduler_token_to_destroy = Scheduler.find(scheduler_to_destroy.id)
          if Scheduler.find(scheduler_to_destroy.id)
            Scheduler.find(scheduler_to_destroy.id).destroy
          end
          last_month = Month.last
          date_to_destroy_end = Date.civil(last_month.year.name, last_month.number, -1).strftime("%Y-%m-%d")
          Scheduler.where(
            repeat_id: scheduler_token_to_destroy.scheduler_token, 
            date_scheduler_start: scheduler_token_to_destroy.date_scheduler_start.strftime("%Y-%m-%d")..date_to_destroy_end
          ).destroy_all
          @notification      = Notification.new
          @notification.desc   = 'Agendamento cancelado.'
          @notification.status   = false
          @notification.user_id = @scheduler.user_id 
          @notification.save

      end
        @scheduler.destroy
        @notification      = Notification.new
        @notification.desc   = 'Agendamento cancelado.'
        @notification.status   = false
        @notification.user_id = @scheduler.user_id 
        @notification.save
        respond_to do |format|
          format.html { redirect_to schedulers_url, notice: 'Agendamento cancelado.' }
          format.json { head :no_content }
        end

      

    else
      respond_to do |format|
        format.html { redirect_to schedulers_url, notice: 'Você não pode cancelar um agendamento com menos de 24h do horário marcado.' }
        format.json { head :no_content }
      end

    end


  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_scheduler
      @scheduler = Scheduler.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def scheduler_params
      params.require(:scheduler).permit(:status, :date_scheduler_start, :time_scheduler_start, :scheduler_token, :user_id, :room_id, :repeat, :repeat_id,:id_father, :confirm, :price, :price_fixed, :month_id)
    end
end
