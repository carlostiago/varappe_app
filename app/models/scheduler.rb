class Scheduler < ApplicationRecord
  belongs_to :user
  belongs_to :room
  belongs_to :month

  validates :date_scheduler_start, presence: true
  validates :time_scheduler_start, presence: true
  validates :room_id, presence: true
  validates :scheduler_token, uniqueness: { message: "Essa sala já está reservada para este horário" }

  validate :hour_min
  validate :date_min
  validate :month_free_only_repeat
  validate :repeat_conflict
  validate :month_not_free_repeat


  def price_pretty
    helper.number_to_currency(price, :unit => "R$ ", separator: ",", delimiter: ".",:precision => 2)
  end

  def price_fixed_pretty
    helper.number_to_currency(price_fixed, :unit => "R$ ", separator: ",", delimiter: ".",:precision => 2)
  end

  private

  def helper
    @helper ||= Class.new do
      include ActionView::Helpers::NumberHelper
    end.new
  end

  validate :date_limit
  before_save :set_price
  after_save :set_profile
  after_destroy :set_profile

  #validates :room_id, uniqueness: { scope: [:date_scheduler_start, :time_scheduler_start], message: "Essa sala já está reservada para este horário"  }
  
  validates :time_scheduler_start, uniqueness: { scope: [:date_scheduler_start, :user_id], message: "Você já tem uma sala reservada para este horário"  }

  before_validation do
     self.scheduler_token =  date_scheduler_start.to_s  + '-' + time_scheduler_start.to_s + '-' + room_id.to_s
  end



  private
  def set_profile
   
    today = Date.today
    ultimo_dia_proximo_mes = Date.civil(today.strftime("%Y").to_i, today.strftime("%m").to_i, -1)
    primeiro_dia_proximo_mes = Date.civil(today.strftime("%Y").to_i, today.strftime("%m").to_i, 1)
    scheduler_count = Scheduler.where(date_scheduler_start: primeiro_dia_proximo_mes..ultimo_dia_proximo_mes, confirm: true).count
    user = User.find(self.user_id)
    scheduler_count = Scheduler.where(user_id: user.id, date_scheduler_start: primeiro_dia_proximo_mes..ultimo_dia_proximo_mes, confirm: true).count
    if scheduler_count >= 4
      user.profile_id = 1
      user.save!
    else
      user.profile_id = 2
      user.save!
    end
  end

  private
  def date_limit
    if self.repeat == false
      if self.user.profile_id == 1
        mes_ativado = Month.where(status: true, year_id: 1).last
      else
        mes_ativado = Month.where(all_user: true, year_id: 1).last
      end
      month = I18n.l(self.date_scheduler_start.to_date, :format => :long, :locale => 'pt-BR')
      if self.date_scheduler_start.strftime("%m").to_i > mes_ativado.number
        errors.add(:date_scheduler_start, "- Você não pode criar um agendamento para essa data porque #{month} ainda não está disponível. Os agendamentos estão disponíveis até #{mes_ativado.month}")
      end
    end
  end

  private
  def date_min
    today = Date.today
    if self.date_scheduler_start < today
      errors.add(:date_scheduler_start, "- Você não pode criar um agendamento para uma data passada")
    end
  end

  private
  def month_free_only_repeat
    
    ultimo_mes_liberado_fixo = Month.where(status: true,year_id: 1).last
    ultimo_mes_liberado_all = Month.where(status: true,all_user: true, year_id: 1).last

    if self.repeat == false && self.date_scheduler_start.strftime('%m').to_i > ultimo_mes_liberado_all.number
      errors.add(:date_scheduler_start, "- Os agendamentos avulsos estão disponíveis até #{ultimo_mes_liberado_all.month}")
    end
  end

  private
  def month_not_free_repeat
    
    if self.user.profile_id == 1
       ultimo_mes_liberado = Month.where(status: true,year_id: 1).last
    else
       ultimo_mes_liberado = Month.where(status: true,all_user: true, year_id: 1).last
    end
   

    mes_consulta = Month.where(number: self.date_scheduler_start.strftime('%m').to_i, year_id: 1).last

    if self.repeat != true && mes_consulta.status == false
      errors.add(:date_scheduler_start, "- Agendamentos não estão liberados para o mês de #{ultimo_mes_liberado.month}.")
    end
  end


  private
  def hour_min
    #now = Time.now - 1.hour
    now = Time.now
    hour_date = self.date_scheduler_start.to_s + ' ' + self.time_scheduler_start.to_s + ':00'
    if hour_date.to_time <= now
      errors.add(:time_scheduler_start, "- Não é possível agendar para este horário")
    end
  end

  private
  def repeat_conflict
    if self.repeat == true
      week_scheduler  = self.date_scheduler_start.cweek 
      week_repeat     = 52 - week_scheduler
      i = 0
      data_conflito = ''
      for i in 1..week_repeat do
        conflito = Scheduler.where(date_scheduler_start: self.date_scheduler_start + (i*7), time_scheduler_start: self.time_scheduler_start, room_id: self.room_id)
        data_conflito = self.date_scheduler_start + (i*7)
        data_conflito_format = data_conflito.strftime("%d") + '/' + data_conflito.strftime("%m") + '/' + data_conflito.strftime("%Y")  
        if conflito.count > 0
          errors.add(:date_scheduler_start, "- Você não pode criar este agendamento com repetição porque há uma data (#{data_conflito_format}) já reservada que conflita com seu agendamento")
        end
      end
    end
  end



  private
  def set_price 
    if self.date_scheduler_start.wday > 0 && self.date_scheduler_start.wday < 6
      if self.time_scheduler_start >= 7 && self.time_scheduler_start  < 12 
      #dia de semana pela manhã para perfil fixo
        self.price_fixed = Price.find(1).value
      #dia de semana pela manhã para perfil avulso
        self.price = Price.find(5).value
      elsif self.time_scheduler_start >= 12 && self.time_scheduler_start  < 17
      #dia de semana pela tarde para perfil fixo
        self.price_fixed = Price.find(2).value
      #dia de semana pela tarde para perfil avulso
        self.price = Price.find(6).value
      else
      #dia de semana pela noite para perfil fixo
        self.price_fixed = Price.find(3).value
      #dia de semana pela noite para perfil avulso
        self.price = Price.find(7).value
      end
    else
    #final de semana para perfil fixo
      self.price_fixed = Price.find(4).value
    #final de semana para perfil avulso
      self.price = Price.find(8).value
    end
  end




# day_id = 1
# time_scheduler_start = 12
# if day_id > 0 && day_id < 7
#   if time_scheduler_start >= 7 && time_scheduler_start  < 12 
#     puts "Valor para fixo dia de semana de manhã R$" + Price.find(1).value.to_s
#     puts "--------------------"
#     puts "Valor para avulso dia de semana de manhã R$" + Price.find(5).value.to_s
#   elsif time_scheduler_start >= 12 && time_scheduler_start  < 17
#     puts "Valor para fixo dia de semana de tarde R$" + Price.find(2).value.to_s
#     puts "--------------------"
#     puts "Valor para avulso dia de semana de tarde R$" + Price.find(6).value.to_s
#   else
#     puts "Valor para fixo dia de semana de noite R$" + Price.find(3).value.to_s
#     puts "--------------------"
#     puts "Valor para avulso dia de semana de noite R$" + Price.find(7).value.to_s
#   end
# else
#     puts "Valor para fixo final de semana R$" + Price.find(4).value.to_s
#     puts "--------------------"
#     puts "Valor para avulso final de semana R$" + Price.find(8).value.to_s
# end

        

          

end