class Price < ApplicationRecord
  belongs_to :profile
  def price_pretty
    helper.number_to_currency(value, :unit => "R$ ", separator: ",", delimiter: ".",:precision => 2)
  end
  private

  def helper
    @helper ||= Class.new do
      include ActionView::Helpers::NumberHelper
    end.new
  end

end
