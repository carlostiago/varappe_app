json.extract! setting, :id, :minimum_of_day, :created_at, :updated_at
json.url setting_url(setting, format: :json)
