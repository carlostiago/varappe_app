json.extract! notification, :id, :desc, :user_id, :status, :read_in, :created_at, :updated_at
json.url notification_url(notification, format: :json)
