json.extract! scheduler, :id, :status, :date_scheduler_start, :date_scheduler_end, :time_scheduler_start, :duration, :scheduler_id, :user_id, :room_id, :created_at, :updated_at
json.url scheduler_url(scheduler, format: :json)
