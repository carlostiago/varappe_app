json.extract! price, :id, :name, :value, :start, :end, :profile_id, :created_at, :updated_at
json.url price_url(price, format: :json)
