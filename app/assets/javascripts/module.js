/*
 * Scripts executados no módulo, como validações de campo, máscaras, alertas, etc.
 */

$(document).ready(function () {
    $("#tab-dados-conta").on("click", function () {
        var currentUrl = window.location.href;
        var param = currentUrl.split('?');

        var newParam = 'tab=dados-conta';
        var newUrl = param[0] + '?' + newParam;

        window.history.pushState(newUrl, "Dados da Conta", newUrl);
    });

    $("#tab-dados-empresa").on("click", function () {
        var currentUrl = window.location.href;
        var param = currentUrl.split('?');

        var newParam = 'tab=dados-empresa';
        var newUrl = param[0] + '?' + newParam;

        window.history.pushState(newUrl, "Dados da Empresa", newUrl);
    });

    $("#tab-configuracoes").on("click", function () {
        var currentUrl = window.location.href;
        var param = currentUrl.split('?');

        var newParam = 'tab=configuracoes';
        var newUrl = param[0] + '?' + newParam;

        window.history.pushState(newUrl, "Configurações", newUrl);
    });

    $("#btn-cadastrar-novo-local").on("click", function () {
        $(this).hide();
    });

    $("#btn-cancel-local").on('click', function () {
        $("#btn-cadastrar-novo-local").show();
    });

    $("#btn-cadastrar-novo-usuario").on("click", function () {
        $(this).hide();
    });

    $("#btn-cancel-usuario").on('click', function () {
        $("#btn-cadastrar-novo-usuario").show();
    });
});

window.onpopstate = function (e) {
    if (e.state) {
        location.href = e.state;
    } else {
        var currentUrl = window.location.href;
        location.href = currentUrl;
    }
};