/*
 * Requisições AJAX feitas no módulo
 */

$(document).ready(function () {
    /**************************************************************************
     * SALVAR DADOS DA CONTA
     *************************************************************************/
    $("#form-dados-conta").on("submit", function () {
        /*
         * Verificação da senha
         */
        var pass1 = $("#senha").val();
        var pass2 = $("#senha2").val();

        if (pass1.length > 0 && pass2.length > 0) {
            if (pass1 !== pass2) {
                notificationError('As senhas precisam ser idênticas!');
                return false;
            }
        } else if ((pass1.length === 0 && pass2.length > 0) || (pass2.length === 0 && pass1.length > 0)) {
            notificationError('Os campos de senha precisam ser preenchidos!');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: BASEDIR + '_actions/dados-conta-save',
            data: $("#form-dados-conta").serialize(),
            success: function (result) {
                var data = jQuery.parseJSON(result);

                if (data.code === '1') {
                    notificationSuccess(data.message);
                } else {
                    notificationError(data.message);
                }
            }
        });

        return false;
    });

    /**************************************************************************
     * SALVAR DADOS DA EMPRESA
     *************************************************************************/
    $("#form-dados-empresa").on("submit", function () {
        $.ajax({
            type: 'POST',
            url: BASEDIR + '_actions/dados-empresa-save',
            data: $("#form-dados-empresa").serialize(),
            success: function (result) {
                var data = jQuery.parseJSON(result);

                if (data.code === '1') {
                    notificationSuccess(data.message);
                } else {
                    notificationError(data.message);
                }
            }
        });

        return false;
    });


    /**************************************************************************
     * CONFIURAÇÕES
     *************************************************************************/
    $("#form-configuracoes").on("submit", function () {
        $.ajax({
            type: 'POST',
            url: BASEDIR + '_actions/configuracoes-save',
            data: $('#form-configuracoes').serialize(),
            success: function (result) {
                var data = jQuery.parseJSON(result);

                if (data.code === '1') {
                    notificationSuccess(data.message);
                } else if (data.code === '0') {
                    notificationError(data.message);
                }
            }
        });

        return false;
    });
});