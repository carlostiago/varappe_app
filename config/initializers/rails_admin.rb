RailsAdmin.config do |config|
  ### Popular gems integration

  ## == Devise ==
   config.authenticate_with do
     warden.authenticate! scope: :user
     if !current_user.admin?
        redirect_to main_app.root_path
      end
   end

  config.model 'Scheduler' do
    list do
      field :id
      field :status
      field :date_scheduler_start
      field :time_scheduler_start
      field :user_id

      field :user_id do
        formatted_value do # used in form views
          user = User.find(value)
          value = user.name
        end

        pretty_value do # used in list view columns and show views, defaults to formatted_value for non-association fields
          user = User.find(value)
          value = user.name
        end

        export_value do
          user = User.find(value)
          value = user.name # used in exports, where no html/data is allowed
        end
      end
      field :user_id, :enum do
        enum do
          User.all.collect {|p| [p.name, p.id]}
        end
      end


      field :room_id do
        formatted_value do # used in form views
          room = Room.find(value)
          value = room.name
        end

        pretty_value do # used in list view columns and show views, defaults to formatted_value for non-association fields
          room = Room.find(value)
          value = room.name
        end

        export_value do
          room = Room.find(value)
          value = room.name # used in exports, where no html/data is allowed
        end
      end


      field :price_pretty do
        label "Preço avulso"
      end
      field :price_fixed_pretty do
        label "Preço fixo"
      end

      field :repeat
      field :reserved
      field :confirm
      field :month_id do
        label "Mês"
      end
      field :created_at
      field :updated_at

    end
  end


  config.model 'Price' do
    list do
      field :id
      field :name

      field :price_pretty do
        label "Preço"
      end


      field :start
      field :end

      field :profile_id do
        formatted_value do # used in form views
          profile = Profile.find(value)
          value = profile.name
        end

        pretty_value do # used in list view columns and show views, defaults to formatted_value for non-association fields
          profile = Profile.find(value)
          value = profile.name
        end

        export_value do
          profile = Profile.find(value)
          value = profile.name # used in exports, where no html/data is allowed
        end
      end

      field :created_at
      field :updated_at

    end
  end

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true
  config.main_app_name = ["Locus Vivet", ""]

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
