Rails.application.routes.draw do
  resources :years
  resources :notifications
  resources :settings
  resources :prices
  resources :profiles
  resources :months
  mount RailsAdmin::Engine => '/console', as: 'rails_admin'
  resources :schedulers
  devise_for :users
  resources :rooms
  root 'schedulers#index'
  get "/pages/:page" => "pages#show"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
