namespace :production do
  desc "Configura o ambiente de produção"
  task setup: :environment do

    puts "Rodando rails db:drop"
    %x(RAILS_ENV=production rails db:drop DISABLE_DATABASE_ENVIRONMENT_CHECK=1)

    puts "Rodando rails db:create"
    %x(RAILS_ENV=production rails db:create DISABLE_DATABASE_ENVIRONMENT_CHECK=1)

    puts "Rodando rails db:migrate"
    %x(RAILS_ENV=production rails db:migrate DISABLE_DATABASE_ENVIRONMENT_CHECK=1)

    puts "Cadastrando Perfis"

    Profile.create!(name: "Fixo")
    Profile.create!(name: "Avulso")

    puts "Perfis cadastrados com sucesso!"
    
    puts "-------------------------------------------------"


    puts "Cadastrando Usuários"

    User.create!(
            name: "Carlos Tiago",
            rg: "1234",
            cpf: "1234",
            date_of_birth: "05/07/1986",
            address: "Rua 4",
            number: "71",
            neighborhood: "Renascer",
            city: "Guanambi",
            state: "Bahia",
            zipcode: "46430-000",
            phone: "(77) 999312982",
            celphone: "(77) 999312982",
            email: "ontiago@gmail.com",
            password: "Nintend0",
            password_confirmation: "Nintend0",
            admin: true,
            profile_id: 2,
            active: true
    )

    User.create!(
            name: "Cliente",
            rg: "551.123-1",
            cpf: "551.061.123-54",
            date_of_birth: "05/07/1986",
            address: "Rua X",
            number: "20",
            neighborhood: "Centro",
            city: "São Paulo",
            state: "São Paulo",
            zipcode: "03425-000",
            phone: "(11) 99999-9999",
            celphone: "(1) 99999-9999",
            email: "cliente@designc22.com.br",
            password: "testecliente",
            password_confirmation: "testecliente",
            admin: false,
            profile_id: 2,
            active: true
    )

    User.create!(
            name: "Admin",
            rg: "551.123-1",
            cpf: "551.061.123-54",
            date_of_birth: "12/11/1999",
            address: "Rua ABCD",
            number: "71",
            neighborhood: "Centro",
            city: "São Paulo",
            state: "São Paulo",
            zipcode: "23430-000",
            phone: "(1) 999999999",
            celphone: "(11) 999999999",
            email: "admin@designc22.com.br",
            password: "testeadmin",
            password_confirmation: "testeadmin",
            admin: true,
            profile_id: 2,
            active: true
    )

    User.create!(
            name: "Cassiano",
            rg: "551.123-1",
            cpf: "551.061.123-54",
            date_of_birth: "12/11/1999",
            address: "Rua ABCD",
            number: "71",
            neighborhood: "Centro",
            city: "São Paulo",
            state: "São Paulo",
            zipcode: "23430-000",
            phone: "(1) 999999999",
            celphone: "(11) 999999999",
            email: "cassiano@designc22.com.br",
            password: "123456",
            password_confirmation: "123456",
            admin: true,
            profile_id: 1,
            active: true
    )

    puts "Usuários cadastrados com sucesso!"
    
    puts "-------------------------------------------------"

    puts "Cadastrando Salas"

    Room.create!(name: "Água", description: "", color: "#286AF7", status: true)
    Room.create!(name: "Terra", description: "", color: "#479161", status: true)
    Room.create!(name: "Fogo", description: "", color: "#FD1919", status: true)
    Room.create!(name: "Ar", description: "", color: "#CCCCCC", status: true)


    puts "Salas cadastradas com sucesso!"

    puts "-------------------------------------------------"

    #puts "Criando Agendamentos"

    #Scheduler.create!(room_id: 1, user_id: 1, date_scheduler_start: "2019-01-03", time_scheduler_start: "7", repeat: false)
    #Scheduler.create!(room_id: 2, user_id: 1, date_scheduler_start: "2019-01-03", time_scheduler_start: "9", repeat: false)
    #Scheduler.create!(room_id: 3, user_id: 1, date_scheduler_start: "2019-01-03", time_scheduler_start: "10", repeat: false)
    #Scheduler.create!(room_id: 4, user_id: 1, date_scheduler_start: "2019-01-03", time_scheduler_start: "14", repeat: false)
    #Scheduler.create!(room_id: 2, user_id: 1, date_scheduler_start: "2019-01-03", time_scheduler_start: "9", repeat: false)
    #Scheduler.create!(room_id: 3, user_id: 1, date_scheduler_start: "2019-01-03", time_scheduler_start: "18", repeat: false)


    #puts "Agendamentos criados com sucesso!"

    puts "-------------------------------------------------"

    puts "Cadastrando Anos"

    Year.create!(name: 2020)

    puts "Anos cadastrados com sucesso!"

    puts "-------------------------------------------------"

    puts "Cadastrando Meses"

    Month.create!(month: "Janeiro"      ,number: 1, year_id: 1, status: true, all_user: true )
    Month.create!(month: "Fevereiro"    ,number: 2, year_id: 1, status: true)
    Month.create!(month: "Março"        ,number: 3, year_id: 1)
    Month.create!(month: "Abril"        ,number: 4, year_id: 1)
    Month.create!(month: "Maio"         ,number: 5, year_id: 1)
    Month.create!(month: "Junho"        ,number: 6, year_id: 1)
    Month.create!(month: "Julho"        ,number: 7, year_id: 1)
    Month.create!(month: "Agosto"       ,number: 8, year_id: 1)
    Month.create!(month: "Setembro"     ,number: 9, year_id: 1)
    Month.create!(month: "Outubro"      ,number: 10, year_id: 1)
    Month.create!(month: "Novembro"     ,number: 11, year_id: 1)
    Month.create!(month: "Dezembro"     ,number: 12, year_id: 1)

    puts "Meses cadastrados com sucesso!"

    puts "-------------------------------------------------"

    puts "Cadastrando Preços"

    Price.create!(name: "Manhã", value: 21, start:7, end:12, profile_id: 1) #fixo
    Price.create!(name: "Tarde", value: 24, start:12, end:17, profile_id: 1) #fixo
    Price.create!(name: "Noite", value: 33, start:17, end:22, profile_id: 1) #fixo
    Price.create!(name: "Sábado, Domingo e Feriados", value: 24, start:7, end:22, profile_id: 1) #fixo
    Price.create!(name: "Manhã", value: 32, start:7, end:12, profile_id: 2) #avulso
    Price.create!(name: "Tarde", value: 34, start:12, end:17, profile_id: 2) #avulso
    Price.create!(name: "Noite", value: 44, start:17, end:22, profile_id: 2) #avulso
    Price.create!(name: "Sábado, Domingo e Feriados", value: 34, start:7, end:22, profile_id: 2) #avulso

    puts "Preços cadastrados com sucesso!"

    puts "-------------------------------------------------"

    puts "Cadastrando Configurações"

    Setting.create!(minimum_of_day: 4)

    puts "Configurações cadastradas com sucesso!"

    puts "-------------------------------------------------"


    # puts "Criando agendamentos para o usuário Cassiano!"



    # 34.times do |i|

    #     @scheduler_repeat = Scheduler.new
    #     @scheduler_repeat.user_id = 4
    #     @scheduler_repeat.time_scheduler_start  = 7
    #     @scheduler_repeat.repeat_id             = "2019-05-02-7-1"
    #     @scheduler_repeat.repeat                = true
    #     @scheduler_repeat.date_scheduler_start  = "2019-05-02".to_date + (i.to_i*7)
    #     @scheduler_repeat.month_id              = 2
    #     @scheduler_repeat.room_id               = 1
    #     mes_agendamento                         = 1
    #     mes_ativado                             = Month.where(status: true, year_id: 1).last

    #     if mes_ativado.number < mes_agendamento 
    #         @scheduler_repeat.reserved = true
    #         @scheduler_repeat.status                = "Agendamento reservado"
    #         @scheduler_repeat.confirm = false
    #     else
    #         @scheduler_repeat.status                = "Agendamento realizado"
    #         @scheduler_repeat.confirm = true
    #     end

    #     @scheduler_repeat.save

    # end


    # 34.times do |i|

    #     @scheduler_repeat = Scheduler.new
    #     @scheduler_repeat.user_id = 4
    #     @scheduler_repeat.time_scheduler_start  = 7
    #     @scheduler_repeat.repeat_id             = "2019-05-02-8-7"
    #     @scheduler_repeat.repeat                = true
    #     @scheduler_repeat.date_scheduler_start  = "2019-05-02".to_date + (i.to_i*7)
    #     @scheduler_repeat.month_id              = 2
    #     @scheduler_repeat.room_id               = 3
    #     mes_agendamento                         = 2
    #     mes_ativado                             = Month.where(status: true, year_id: 1).last

    #     if mes_ativado.number < mes_agendamento 
    #         @scheduler_repeat.reserved = true
    #         @scheduler_repeat.status                = "Agendamento reservado"
    #         @scheduler_repeat.confirm = false
    #     else
    #         @scheduler_repeat.status                = "Agendamento realizado"
    #         @scheduler_repeat.confirm = true
    #     end

    #     @scheduler_repeat.save

    # end


    # 3.times do |i|

    #     @scheduler_repeat = Scheduler.new
    #     @scheduler_repeat.user_id = 4
    #     @scheduler_repeat.time_scheduler_start  = 7
    #     @scheduler_repeat.date_scheduler_start  = "2019-05-02".to_date + (i.to_i*7)
    #     @scheduler_repeat.month_id              = 2
    #     @scheduler_repeat.room_id               = 3
    #     @scheduler_repeat.status                = "Agendamento realizado"
    #     @scheduler_repeat.confirm = true
    #     @scheduler_repeat.save

    # end

    # 2.times do |i|

    #     @scheduler_repeat = Scheduler.new
    #     @scheduler_repeat.user_id = 4
    #     @scheduler_repeat.time_scheduler_start  = 7
    #     @scheduler_repeat.date_scheduler_start  = "2019-05-02".to_date + (i.to_i*7)
    #     @scheduler_repeat.month_id              = 2
    #     @scheduler_repeat.room_id               = 3
    #     @scheduler_repeat.status                = "Agendamento realizado"
    #     @scheduler_repeat.confirm = true
    #     @scheduler_repeat.save

    # end

    # puts "Agendamentos cadastradas com sucesso!"

    # puts "-------------------------------------------------"


  end

end
