# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190108154828) do

  create_table "months", force: :cascade do |t|
    t.string "month"
    t.integer "number"
    t.boolean "status", default: false
    t.boolean "all_user", default: false
    t.integer "year_id"
    t.boolean "close", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["year_id"], name: "index_months_on_year_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.text "desc"
    t.integer "user_id"
    t.boolean "status"
    t.datetime "read_in"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "prices", force: :cascade do |t|
    t.string "name"
    t.decimal "value", precision: 14, scale: 2
    t.integer "start"
    t.integer "end"
    t.integer "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["profile_id"], name: "index_prices_on_profile_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "color"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedulers", force: :cascade do |t|
    t.string "status"
    t.date "date_scheduler_start"
    t.integer "time_scheduler_start"
    t.integer "user_id"
    t.integer "room_id"
    t.decimal "price", precision: 14, scale: 2
    t.decimal "price_fixed", precision: 14, scale: 2
    t.string "scheduler_token"
    t.boolean "repeat"
    t.boolean "reserved", default: false
    t.string "repeat_id"
    t.integer "id_father"
    t.boolean "confirm", default: false
    t.integer "month_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["month_id"], name: "index_schedulers_on_month_id"
    t.index ["room_id"], name: "index_schedulers_on_room_id"
    t.index ["user_id"], name: "index_schedulers_on_user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.integer "minimum_of_day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", default: "", null: false
    t.integer "profile_id", default: 2
    t.boolean "active", default: false
    t.boolean "admin", default: false
    t.string "rg", null: false
    t.string "cpf", null: false
    t.date "date_of_birth", null: false
    t.string "address", null: false
    t.string "number", null: false
    t.string "neighborhood", null: false
    t.string "city", null: false
    t.string "state", null: false
    t.string "zipcode", null: false
    t.string "phone", null: false
    t.string "celphone", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["profile_id"], name: "index_users_on_profile_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "years", force: :cascade do |t|
    t.integer "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
