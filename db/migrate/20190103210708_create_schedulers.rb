class CreateSchedulers < ActiveRecord::Migration[5.1]
  def change
    create_table :schedulers do |t|
      t.string :status
      t.date :date_scheduler_start
      t.integer :time_scheduler_start
      t.references :user, foreign_key: true
      t.references :room, foreign_key: true
      t.decimal :price, :precision => 14, :scale => 2
      t.decimal :price_fixed, :precision => 14, :scale => 2
      t.string :scheduler_token
      t.boolean :repeat
      t.boolean :reserved,default: false
      t.string :repeat_id
      t.integer :id_father
      t.boolean :confirm, default: false
      t.references :month, foreign_key: true

      

      #t.references :month, foreign_key: true

      t.timestamps
    end
  end
end
