class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.text :desc
      t.references :user, foreign_key: true
      t.boolean :status
      t.datetime :read_in

      t.timestamps
    end
  end
end
