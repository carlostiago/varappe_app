class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.integer :minimum_of_day

      t.timestamps
    end
  end
end
