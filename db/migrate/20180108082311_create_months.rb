class CreateMonths < ActiveRecord::Migration[5.1]
  def change
    create_table :months do |t|
      t.string :month
      t.integer :number
      t.boolean :status, default: false
      t.boolean :all_user, default: false
      t.references :year, foreign_key: true
      t.boolean :close, default: false

      t.timestamps

    end
  end
end
