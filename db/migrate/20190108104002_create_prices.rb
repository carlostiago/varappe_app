class CreatePrices < ActiveRecord::Migration[5.1]
  def change
    create_table :prices do |t|
      t.string :name
      t.decimal :value, :precision => 14, :scale => 2
      t.integer :start
      t.integer :end
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
